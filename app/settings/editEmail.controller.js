angular.module('app').controller('edit.email.controller', ['$uibModalInstance', 'email', '$scope', 
 function($uibModalInstance, email, $scope) {
	$scope.form = email;

	$scope.submit = function() {
		$scope.editEmailForm.$setSubmitted();
		if (!$scope.editEmailForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);