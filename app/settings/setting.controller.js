angular.module('app').controller('setting.controller', ['$scope', 'http.service', 'settingAPIURL', 'toast.service', '$uibModal',
    function ($scope, httpService, settingAPIURL, toastService, $uibModal) {
        var self = this;

        //initial
        $scope.transactions = [];
        $scope.questions = [];
        $scope.emails = [];
        $scope.about = '';
        $scope.terms = '';
        $scope.system = null;
        $scope.form = {
            date: null,
            action: '',
            amount: null,
            page: 1,
            numPages: 1,
            total: 0,
            numItems: 20
        };

        $scope.mapping = {
            'UserBoughtCard': 'Compró',
            'Refund': 'Reembolso',
            'Ticket': 'Alerta de oferta',
            'Auction': 'Subasta ganada',
            'VendorPaid': 'Dinero del vendedor recibido',
            'VendorCredit': 'El proveedor agrega crédito al usuario',
            'AdminCredit': 'Admin agregar crédito al usuario'
        };

        $scope.openDate = function () {
            $scope.popup.opened = true;
        };

        //query
        $scope.getQuestions = function () {
            httpService.doGet(settingAPIURL.question).then(function (questions) {
                $scope.questions = questions;
            });
        };

        $scope.getEmails = function () {
            httpService.doGet(settingAPIURL.emails).then(function (emails) {
                $scope.emails = emails;
            });
        };

        $scope.resync = function () {
            $scope.form.page = 1;
        };

        $scope.getTransactions = function () {
            if ($scope.form.date) {
                $scope.form.date = $scope.form.date.toISOString().slice(0, 10);
            }
            const queryStr = httpService.buildQuery($scope.form);
            httpService.doGet(settingAPIURL.transaction + '?' + queryStr).then(function (transactions) {
                $scope.transactions = transactions.items;
                $scope.form.page = transactions.page;
                $scope.form.numPages = transactions.num_pages;
                $scope.form.currentPage = transactions.page;
                $scope.form.total = transactions.num_pages * $scope.form.numItems;
            });
        };

        $scope.pageChanged = function () {
            $scope.getTransactions();
        };

        $scope.getSystem = function () {
            httpService.doGet(settingAPIURL.system).then(function (system) {
                $scope.system = system;
            });
        };

        httpService.doGet(settingAPIURL.about).then(function (about) {
            $scope.about = about;
        });

        httpService.doGet(settingAPIURL.terms).then(function (terms) {
            $scope.terms = terms;
        });

        //calls
        $scope.getQuestions();
        $scope.getEmails();
        $scope.getSystem();
        $scope.getTransactions();

        //buttons
        $scope.saveAbout = function () {
            if ($scope.about.description.length > 0) {
                httpService.doPost(settingAPIURL.about, $scope.about).then(function (about) {
                    $scope.about = about;
                    toastService.success();
                });
            }
        };
        $scope.saveTerms = function () {
            if ($scope.terms.description.length > 0) {
                httpService.doPost(settingAPIURL.termsEdit, $scope.terms).then(function (terms) {
                    $scope.terms = terms;
                    toastService.success();
                });
            }
        };

        $scope.closeModal = function () {
            $scope.modalInstance.dismiss('cancel');
        };

        $scope.openModal = function (callback) {
            $scope.modalInstance.result.then(function (result) {
                callback(result);
            });
        };

        $scope.addQuestion = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'settings/addQuestion.html',
                controller: 'add.question.controller',
                controllerAs: 'vm',
                size: 'lg'
            });

            modalInstance.result.then(function (form) {
                self.selected = form;
                httpService.doPost(settingAPIURL.questionCreate, form).then(function () {
                    toastService.success();
                    $scope.getQuestions();
                });
            });
        };

        $scope.editEmail = function (email) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'settings/editEmail.html',
                controller: 'edit.email.controller',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    email: function () {
                        return email;
                    }
                }
            });

            modalInstance.result.then(function (form) {
                form.id = email.id;
                httpService.doPost(settingAPIURL.emailEdit, form).then(function () {
                    toastService.success('Actualizado');
                    $scope.getEmails();
                });
            });
        };

        $scope.editQuestion = function (question) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'settings/editQuestion.html',
                controller: 'edit.question.controller',
                controllerAs: 'vm',
                resolve: {
                    question: function () {
                        return question;
                    }
                },
                size: 'lg'
            });

            modalInstance.result.then(function (form) {
                form.id = question.id;
                httpService.doPost(settingAPIURL.questionEdit, form).then(function () {
                    toastService.success('Actualizado');
                    $scope.getQuestions();
                });
            });
        };
        $scope.deleteQuestion = function (id) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'core/confirmationModal.html',
                controller: 'confirmation.controller',
                controllerAs: 'md',
                size: 'lg'
            });

            modalInstance.result.then(function () {
                httpService.doDelete(settingAPIURL.questionDelete + '?id=' + id).then(function () {
                    toastService.success('Eliminado');
                    $scope.getQuestions();
                });
            });
        };

        $scope.viewTransaction = function (transaction) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'settings/viewTransaction.html',
                controller: 'view.transaction.controller',
                controllerAs: 'vm',
                resolve: {
                    transaction: function () {
                        return transaction;
                    }
                },
                size: 'lg'
            });
            modalInstance.result.then(function (form) { });
        };

    }]).constant('settingAPIURL', {
        question: '/v1/questions',
        questionEdit: '/v1/questionedit',
        questionCreate: '/v1/questioncreate',
        questionDelete: '/v1/questiondelete',
        about: '/v1/about',
        terms: '/v1/terms',
        termsEdit: '/v1/termsedit',
        emails: '/v1/emails',
        emailEdit: '/v1/emailedit',
        system: '/v1/system',
        transaction: '/v1/transaction'
    });