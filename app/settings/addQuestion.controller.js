angular.module('app').controller('add.question.controller', ['$uibModalInstance', '$scope', 
 function($uibModalInstance, $scope) {
	$scope.form = {
		question: '',
		answer: ''
	};

	$scope.submit = function() {
		$scope.addQuestionForm.$setSubmitted();
		if (!$scope.addQuestionForm.$invalid) {
			$uibModalInstance.close(this.form);
		}
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);