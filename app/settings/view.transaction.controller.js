angular.module('app').controller('view.transaction.controller', ['$uibModalInstance', 'transaction', '$scope', 
 function($uibModalInstance, transaction, $scope) {
	$scope.transaction = transaction;

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);