angular.module('app').controller('edit.question.controller', ['$uibModalInstance', 'question', '$scope', 
 function($uibModalInstance, question, $scope) {
	$scope.form = question;

	$scope.submit = function() {
		$scope.editQuestionForm.$setSubmitted();
		if (!$scope.editQuestionForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);