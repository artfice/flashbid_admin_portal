angular.module('app')
.constant('mockQuestion', [{
        id: 1,
        question: 'What is FlashBid?',
        answer: 'blah blah blah'
    }, {
        id: 1,
        question: 'How does it work?',
        answer: 'blah blah blah'
    }])
.constant('mockEmail', [{
        id: 1,
        slug: 'register',
        subject: 'Thanks for registering'
    }, {
        id: 2,
        slug: 'reset-password',
        subject: 'Please Reset your password'
    }, {
        id: 3,
        slug: 'confirm-email',
        subject: 'Confirm your Email'
    }, {
        id: 4,
        slug: 'bid',
        subject: 'Bidding starts in 2 minutes'
    }, {
        id: 5,
        slug: 'winner',
        subject: 'Person X won the auctions'
    }, {
        id: 6,
        slug: 'credit-added',
        subject: 'We added flashbid coins to your app'
    } , {
        id: 7,
        slug: 'credit-deducted',
        subject: 'We deducted flash coins so you can win auction'
    } , {
        id: 7,
        slug: 'credit-refund',
        subject: 'We refunded you flashbid coins'
    }  , {
        id: 7,
        slug: 'winner-code',
        subject: 'You Won item X'
    } ])
.constant('mockTransaction', [{
        id: 1,
        date: '2016-09-10',
        action: 'Bought',
        amount: 100,
        userOldTotal: 0,
        userTotal: 100,
        systemOldTotal: 0,
        systemTotal: 0,
        userId: 1,
        notes: 'Flash Card Claimed 1234-5678-8901'
    }, {
        id: 2,
        date: '2016-09-10',
        action: 'Refund',
        amount: 100,
        userOldTotal: 100,
        userTotal: 200,
        systemOldTotal: 0,
        systemTotal: 0,
        userId: 1,        
        notes: 'Flash Card Claimed 1234-5678-8901'
    }, {
        id: 2,
        date: '2016-09-10',
        action: 'Ticket',
        amount: 10,
        userOldTotal: 200,
        userTotal: 190,
        systemOldTotal: 0,
        systemTotal: 10,
        userId: 1,        
        notes: ''
    }, {
        id: 2,
        date: '2016-09-10',
        action: 'Bought',
        amount: 100,
        userOldTotal: 190,
        userTotal: 90,
        systemOldTotal: 0,
        systemTotal: 110,
        notes: '',
        userId: 1        
    }]);