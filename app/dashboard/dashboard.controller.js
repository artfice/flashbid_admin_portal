angular.module('app').controller('dashboard.controller', ['$scope', 'http.service', 'itemAPIURL', 
function ($scope, httpService, itemAPIURL) {
    $scope.items = [];
    $scope.form = {
        page : 1,
        total : 1,
        numItems : 20,
        title: null,
        id: null,
        initialBid: null,
        type: null,
        status: null,
        winCode: null
    };    
    $scope.mapping = {
        'Lost': 'Perdió',
        'Draft': 'Borrador',
        'Active': 'Activo',
        'Flash': 'Destello',
        'Complete': 'Completar',
        'Claimed': 'Reclamado',
        'Unlisted':'Artículo no cotizado'
    };    
    $scope.getItems = function () {
        const queryStr = httpService.buildQuery($scope.form);
        httpService.doGet(itemAPIURL.getAll + '?' + queryStr).then(function(items) {
            $scope.items = items.items;
            $scope.form.page = items.page;
            $scope.form.total = items.num_pages * $scope.form.numItems;
        })        
    };
    $scope.pageChanged = function () {
        $scope.getItems();
    }        
    $scope.getItems();
}]);