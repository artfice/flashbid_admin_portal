angular.module('app', [
 'ui.router',
 'ui.bootstrap',
 'toastr',
 'ngCookies',
 'templates' 
]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 
function($stateProvider, $urlRouterProvider, $httpProvider){
    $stateProvider.state('layout.userProfile', {
        url : '/userProfile/:id',
        templateUrl: 'users/userProfile.html',
        controller: 'user.profile.controller'
    });         
    $stateProvider.state('logout', {
        url : '/logout',
        templateUrl: 'login/logout.html',
        controller: 'logout.controller'
    });    
    $stateProvider.state('login', {
        url : '/login',
        templateUrl: 'login/login.html',
        controller: 'login.controller'
    });
    $stateProvider.state('layout', {
        templateUrl: 'layout.html'
    });
    $stateProvider.state('layout.dashboard', {
        url : '/dashboard',
        templateUrl: 'dashboard/dashboard.html',
        controller: 'dashboard.controller'
    });    
    $stateProvider.state('layout.user', {
        url : '/users',
        templateUrl: 'users/user.html',
        controller: 'user.controller'
    });    
    $stateProvider.state('layout.bid', {
        url : '/bids',
        templateUrl: 'bids/bid.html',
        controller: 'bid.controller'
    });    
    $stateProvider.state('layout.items', {
        url : '/items',
        templateUrl: 'items/item.html',
        controller: 'item.controller'
    });    
    $stateProvider.state('layout.itemsview', {
        url : '/itemsView/:id',
        templateUrl: 'items/itemView.html',
        controller: 'item.view.controller'
    });    
    $stateProvider.state('layout.settings', {
        url : '/settings',
        templateUrl: 'settings/setting.html',
        controller: 'setting.controller' 
    });    
    $urlRouterProvider.otherwise('/login');

    $httpProvider.interceptors.push(['$q', '$location', '$cookies', 'toast.service', 
    function ($q, $location, $cookies, toastService) {
    return {
        'request': function (config) {
            config.headers = config.headers || {};
            const token = $cookies.get('access_token') || '';
            if (token.length > 0) {
                config.headers.Authorization = token;
            }
            return config;
        },
        'responseError': function (response) {
            console.log('error response', response);
            if (response.status == 401 || response.status == -1) {
                $location.path('/login');
                toastService.error('Has sido desconectado o no tienes acceso a este portal. Inicie sesión nuevamente.');
            }
            return $q.reject(response);
        }
    };
    }]);    
}])
.constant('baseUrl', 'https://7gr1kd8ssc.execute-api.us-east-1.amazonaws.com/production')
.constant('env', 'local')
.run([function () {}]);