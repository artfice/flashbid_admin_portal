angular.module('app').controller('login.controller', ['$scope', 'http.service', 'loginAPIURL', 'toast.service', '$uibModal', '$state', '$cookies',
function ($scope, httpService, loginAPIURL, toastService, $uibModal, $state, $cookies) {
    $scope.form = {
        email: '',
        password: ''
	};

	$scope.submitForm = function (isValid) {
		if (!$scope.loginForm.$invalid) {
			httpService.doPost(loginAPIURL.authenticate, $scope.form).then(function(result) {
                if (result.statusCode == 403) {
                    toastService.error(result.message);    
                } else {
                    toastService.success('Conectado');
                    $cookies.put('access_token', result.access_token);
                    $state.go('layout.dashboard');
                }
            });
		}
	}
}]).constant('loginAPIURL', {
    authenticate: '/v1/authenticate'
});
