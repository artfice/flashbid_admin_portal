angular.module('app').controller('logout.controller', ['$scope', 'http.service', 'logoutAPIURL', '$state', '$cookies', 'toast.service',
function ($scope, httpService, logoutAPIURL, $state, $cookies, toastService) {
    $cookies.remove('access_token');
    httpService.doGet(logoutAPIURL.logout).then(function(result) {
        if (result.statusCode == 403) {
            toastService.error(result.message);
            $state.go('layout.dashboard');    
        } else {
            $state.go('login');
        }
    });
}]).constant('logoutAPIURL', {
    logout: '/v1/logout'
});
