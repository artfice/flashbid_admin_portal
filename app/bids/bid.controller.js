angular.module('app').controller('bid.controller', ['$scope', 'http.service', 'bidAPIURL', 'toast.service', '$uibModal', 
function ($scope, httpService, bidAPIURL, toastService, $uibModal) {
    $scope.items = [];
    $scope.form = {
        page : 1,
        total : 1,
        numItem : 20,
        code: ''
    };

    $scope.getBidCodes = function () {
        const queryStr = httpService.buildQuery($scope.form);
        httpService.doGet(bidAPIURL.getBids + '?' + queryStr).then(function(bids) {
            $scope.items = bids.items;
            $scope.form.page = bids.page;
            $scope.form.total = bids.num_pages * $scope.form.numItem;
        });           
    };    

    $scope.pageChanged = function () {
        $scope.getBidCodes();
    };

    $scope.revoke = function (item) {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'core/confirmationModal.html',
            controller: 'confirmation.controller',
            controllerAs: 'md',
            size: 'lg'
            });

            modalInstance.result.then(function () {
                 httpService.doGet(bidAPIURL.revoke + '?id=' + item.id).then(function(result) {
                    if (result.statusCode == 403) {
                        toastService.error(result.message);    
                    } else {
                        toastService.error('Revoked');
                        $scope.getBidCodes();
                    }
                });   
            });        
               
    };

    $scope.bonus = function (item) {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'bids/changeBonus.html',
            controller: 'edit.bid.controller',
            controllerAs: 'vm',
            size: 'lg',
            resolve: {
                bid: function () {
                    return item;
                }
            }            
            });

            modalInstance.result.then(function (form) {
                httpService.doPost(bidAPIURL.bonus + '?id=' + form.id, form).then(function(result) {
                    if (result.statusCode == 403) {
                        toastService.error(result.message);    
                    } else {
                        toastService.success('Updated');
                        $scope.getBidCodes();
                    }
                });
            });           
    };    

    $scope.updateSearch = function () {
        if ($scope.form.code.length > 15) {
            $scope.getBidCodes();        
        }
    };

    $scope.getBidCodes();
}]).constant('bidAPIURL', {
    getBids: '/v1/bidsget',
    revoke: '/v1/bidrevoke',
    bonus: '/v1/bidbonus'
});