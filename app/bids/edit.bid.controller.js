angular.module('app').controller('edit.bid.controller', ['$uibModalInstance', 'bid', '$scope', 
 function($uibModalInstance, bid, $scope) {
	$scope.form = bid;

	$scope.submit = function() {
		$scope.editBidForm.$setSubmitted();
		if (!$scope.editBidForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);