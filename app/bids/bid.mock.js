angular.module('app')
.constant('mockBid', [{
        code: '5436-5678-4587',
        amount: 500,
        bonus: 30,
        userId: 0,
        status: 'new'
    }, {
        code: '5436-5678-4587',
        amount: 500,
        bonus: 30,
        userId: 1,
        status: 'claimed'
    }, {
        code: '5436-5678-4587',
        amount: 500,
        bonus: 30,
        userId: 0,
        status: 'revoked'
    }]);