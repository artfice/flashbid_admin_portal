angular.module('app').controller('item.controller', ['$scope', 'http.service', 'itemAPIURL', 'toast.service', '$uibModal', 'userAPIURL', '$state',
    function ($scope, httpService, itemAPIURL, toastService, $uibModal, userAPIURL, $state) {
        $scope.items = [];
        $scope.vendors = [];
        $scope.form = {
            page: 1,
            total: 1,
            numItems: 20,
            title: null,
            id: null,
            initialBid: null,
            type: null,
            status: null,
            winCode: null
        };
        $scope.mapping = {
            'Lost': 'Perdió',
            'Draft': 'Borrador',
            'Active': 'Activo',
            'Flash': 'Destello',
            'Complete': 'Completar',
            'Claimed': 'Reclamado',
            'Unlisted': 'Artículo no cotizado'
        };
        $scope.getItems = function (isSearch) {
            if (isSearch) {
                $scope.form.page = 1;
                $scope.form.total = 1;
                $scope.form.numItems = 20;
            }
            const queryStr = httpService.buildQuery($scope.form);
            httpService.doGet(itemAPIURL.getAll + '?' + queryStr).then(function (items) {
                $scope.items = items.items;
                $scope.form.page = items.page;
                $scope.form.total = items.num_pages * $scope.form.numItems;
            })
        };

        $scope.getUsers = function () {
            const queryStr = httpService.buildQuery({
                page: 1,
                total: 1,
                numItems: 10000,
                role: 'Distributor'
            });
            httpService.doGet(userAPIURL.getAll + '?' + queryStr).then(function (users) {
                $scope.vendors = users.items;
            })
        };

        $scope.addLottery = function () {
            setTimeout(function () {
                var modalInstance = $uibModal.open({
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'items/addLottery.html',
                    controller: 'add.lottery.controller',
                    controllerAs: 'vm',
                    size: 'lg',
                    resolve: {
                        vendors: function () {
                            return $scope.vendors;
                        }
                    }
                });

                modalInstance.result.then(function (form) {
                    httpService.doPost(itemAPIURL.create, form).then(function (result) {
                        if (result.statusCode == 403) {
                            toastService.error(result.message);
                        } else {
                            toastService.success('Elemento añadido');
                            $scope.getItems();
                        }
                    });
                });
            }, 3000);
        };
        $scope.addAuction = function () {
            setTimeout(function () {
                var modalInstance = $uibModal.open({
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'items/addAuction.html',
                    controller: 'add.auction.controller',
                    controllerAs: 'vm',
                    size: 'lg',
                    resolve: {
                        vendors: function () {
                            return $scope.vendors;
                        }
                    }
                });

                modalInstance.result.then(function (form) {
                    httpService.doPost(itemAPIURL.create, form).then(function (result) {
                        if (result.statusCode == 403) {
                            toastService.error(result.message);
                        } else {
                            toastService.success('Elemento añadido');
                            $scope.getItems();
                        }
                    });
                });
            }, 3000);
        };

        $scope.edit = function (item) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'items/editItem.html',
                controller: 'edit.item.controller',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    item: function () {
                        return item;
                    },
                    vendors: function () {
                        return $scope.vendors;
                    }
                }
            });

            modalInstance.result.then(function (form) {
                httpService.doPost(itemAPIURL.edit + '/' + item.id, form).then(function (result) {
                    if (result.statusCode == 403) {
                        toastService.error(result.message);
                    } else {
                        toastService.success('Elemento añadido');
                        $scope.getItems();
                    }
                });
            });
        };
        $scope.endItem = function (item) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'core/confirmationModal.html',
                controller: 'confirmation.controller',
                controllerAs: 'md',
                size: 'lg'
            });

            modalInstance.result.then(function () {
                httpService.doGet(itemAPIURL.end + '/' + item.id).then(function () {
                    toastService.success('Artículo listado');
                    $scope.getItems();
                });
            });
        };
        $scope.listItem = function (item) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'core/confirmationModal.html',
                controller: 'confirmation.controller',
                controllerAs: 'md',
                size: 'lg'
            });

            modalInstance.result.then(function () {
                httpService.doGet(itemAPIURL.list + '/' + item.id).then(function () {
                    toastService.success('Artículo listado');
                    $scope.getItems();
                });
            });
        };

        $scope.flash = function (item) {
            if (item.ticket_sold < 1) {
                toastService.success('Sin alerta de ofertas compradas. No podemos iniciar la subasta');
                return false;
            }
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'core/confirmationModal.html',
                controller: 'confirmation.controller',
                controllerAs: 'md',
                size: 'lg'
            });

            modalInstance.result.then(function () {
                httpService.doGet(itemAPIURL.flash + '/' + item.id).then(function () {
                    toastService.success('Alerta instantánea ahora');
                    $scope.getItems();
                });
            });
        };        
        $scope.relist = function (item) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'core/confirmationModal.html',
                controller: 'confirmation.controller',
                controllerAs: 'md',
                size: 'lg'
            });

            modalInstance.result.then(function () {
                httpService.doGet(itemAPIURL.relist + '/' + item.id).then(function () {
                    toastService.success('Relíquelo');
                    $scope.getItems();
                });
            });
        };

        $scope.claimPrizeModal = function (item) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'core/confirmationModal.html',
                controller: 'confirmation.controller',
                controllerAs: 'md',
                size: 'lg'
            });

            modalInstance.result.then(function () {
                $scope.claimPrize(item);
            });
        };

        $scope.claimPrize = function (item) {
            httpService.doGet(itemAPIURL.claim + '?id=' + item.id + '&code=' + item.winner_code + '&winner_id=' + item.winner_id).then(function (result) {
                if (result.statusCode == 403) {
                    toastService.error(result.message);
                } else {
                    toastService.success('Actualizado');
                    $scope.getItems();
                }
            });
        };

        $scope.pageChanged = function () {
            $scope.getUsers();
            $scope.getItems();
        }
        $scope.getItems();
        $scope.getUsers();

    }]).constant('itemAPIURL', {
        getAll: '/v1/itemsget',
        edit: '/v1/items',
        create: '/v1/itemcreate',
        list: '/v1/itemlist',
        flash: '/v1/itemflash',
        relist: '/v1/items/relist',
        claim: '/v1/users/claim',
        end: '/v1/itemend'
    });