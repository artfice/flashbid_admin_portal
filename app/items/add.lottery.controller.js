angular.module('app').controller('add.lottery.controller', ['$uibModalInstance', '$scope', 'vendors', 'toast.service',
 function($uibModalInstance, $scope, vendors, toastService) {
	$scope.form = {
		type: 'l',
		initial_bid: 0,
		vendor_id: 0,
		size: 'Small',
		bid_amount: 0,
		item_worth: 10
	};

	$scope.vendorList = vendors.map(function(vendor){
		return {
			value: vendor.id,
			key: vendor.group_name
		};
	});

	AWS.config.update({
		accessKeyId: 'AKIAIOAE7XB65XOIDRBA',
		secretAccessKey: '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'
	});
	AWS.config.region = 'us-east-1';
	var bucket = new AWS.S3({ params: { Bucket: 'images.flashbid' } });	

	$scope.submitForm = function (isValid) {
		console.log('submit form', $scope);
		if(!$scope.file || !$scope.file2) {
			return false;
		}

		var params = { 
			Key: $scope.file.name, 
			ContentType: $scope.file.type, 
			Body: $scope.file, 
			ServerSideEncryption: 'AES256' 
		};

		var secondaryParams = { 
			Key: $scope.file2.name, 
			ContentType: $scope.file2.type, 
			Body: $scope.file2, 
			ServerSideEncryption: 'AES256' 
		};		
		
		bucket.putObject(params, function(err, data) {
			if(err) {
				// There Was An Error With Your S3 Config
				toastService.error(err.message);  
				return false;
			} else {
				$scope.form.image = 'https://s3.amazonaws.com/images.flashbid/' + params.Key;
				console.log('primary data', err, data);
				bucket.putObject(secondaryParams, function(err, data2) {
					if(err) {
						// There Was An Error With Your S3 Config
						toastService.error(err.message);  
						return false;
					} else {
						$scope.form.secondary_image = 'https://s3.amazonaws.com/images.flashbid/' + secondaryParams.Key;
						console.log('secondary data', err, data2);
						$uibModalInstance.close($scope.form);					
					}
				});								
			}
		});
	}

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);