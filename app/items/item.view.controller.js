angular.module('app').controller('item.view.controller', ['$scope', 'http.service', 'itemViewURL',
    '$stateParams', '$uibModal', 'toast.service', function ($scope, httpService, itemViewURL, $stateParams, $uibModal, toastService) {
        $scope.container = null; 
        $scope.itemUser = {
            id: $stateParams.id,
            page: 1,
            total: 1,
            numItems: 50,
            items: []
        };        
        $scope.itemBidders = {
            id: $stateParams.id,
            page: 1,
            total: 1,
            numItems: 50,
            items: []
        };        
        $scope.mapping = {
            'Lost': 'Perdió',
            'Draft': 'Borrador',
            'Active': 'Activo',
            'Flash': 'Destello',
            'Complete': 'Completar',
            'Claimed': 'Reclamado',
            'Unlisted': 'Artículo no cotizado'
        };
        $scope.getItem = function () {
            httpService.doGet(itemViewURL.getOne + '/' + $stateParams.id).then(function (item) {
                $scope.container = item;
            });
        };

        $scope.getItemUsers = function () {
            const queryStr = httpService.buildQuery($scope.itemUser);
            httpService.doGet(itemViewURL.itemUser + '?' + queryStr).then(function (items) {
                $scope.itemUser.items = items.items;
                $scope.itemUser.page = items.page;
                $scope.itemUser.total = items.num_pages * $scope.itemUser.numItems;
            })
        };        
        $scope.getItemBidders = function () {
            const queryStr = httpService.buildQuery($scope.itemBidders);
            httpService.doGet(itemViewURL.itemBidders + '?' + queryStr).then(function (items) {
                $scope.itemBidders.items = items.items;
                $scope.itemBidders.page = items.page;
                $scope.itemBidders.total = items.num_pages * $scope.itemBidders.numItems;
            })
        };        
        $scope.userPageChanged = function () {
            $scope.getItemUsers();
        };
        $scope.bidPageChanged = function () {
            $scope.getItemBidders();
        };
        $scope.getItem();
        $scope.getItemUsers();
        $scope.getItemBidders()
    }]).constant('itemViewURL', {
        getOne: '/v1/items',
        itemUser: '/v1/itemusers',
        itemBidders: '/v1/itemsbids'
    });