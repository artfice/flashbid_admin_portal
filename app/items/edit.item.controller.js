angular.module('app').controller('edit.item.controller', ['$uibModalInstance', '$scope', 'item', 'vendors', 'toast.service',
 function($uibModalInstance, $scope, item, vendors, toastService) {
	$scope.form = {
		title: item.title,
		initial_bid: item.initial_bid,
		item_worth: item.item_worth,
		ticket_amount: item.ticket_amount,
		bid_amount: item.bid_amount,
		type: item.type,
		description: item.description,
		winner_instructions: item.winner_instructions,
		size: item.size
	};
	$scope.vendorName = vendors.filter(function(vendor){
		return vendor.id == item.vendor_id ? vendor: false;
	})[0]['group_name'];

	AWS.config.update({
		accessKeyId: 'AKIAIOAE7XB65XOIDRBA',
		secretAccessKey: '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'
	});
	AWS.config.region = 'us-east-1';
	var bucket = new AWS.S3({ params: { Bucket: 'images.flashbid' } });	

	$scope.submitForm = function (isValid) {
		if($scope.file && $scope.file2) {
			var params = { 
				Key: $scope.file.name, 
				ContentType: $scope.file.type, 
				Body: $scope.file, 
				ServerSideEncryption: 'AES256' 
			};

			var secondaryParams = { 
				Key: $scope.file2.name, 
				ContentType: $scope.file2.type, 
				Body: $scope.file2, 
				ServerSideEncryption: 'AES256' 
			};		
			
			bucket.putObject(params, function(err, data) {
				if(err) {
					// There Was An Error With Your S3 Config
					toastService.error(err.message);  
					return false;
				} else {
					$scope.form.image = 'https://s3.amazonaws.com/images.flashbid/' + params.Key;
					console.log('primary data', err, data);
					bucket.putObject(secondaryParams, function(err, data2) {
						if(err) {
							// There Was An Error With Your S3 Config
							toastService.error(err.message);  
							return false;
						} else {
							$scope.form.secondary_image = 'https://s3.amazonaws.com/images.flashbid/' + secondaryParams.Key;
							console.log('secondary data', err, data2);
							$uibModalInstance.close($scope.form);					
						}
					});								
				}
			});
		} else if($scope.file) {
			var params = { 
				Key: $scope.file.name, 
				ContentType: $scope.file.type, 
				Body: $scope.file, 
				ServerSideEncryption: 'AES256' 
			};
			bucket.putObject(params, function(err, data) {
				if(err) {
					// There Was An Error With Your S3 Config
					toastService.error(err.message);  
					return false;
				} else {
					$scope.form.image = 'https://s3.amazonaws.com/images.flashbid/' + params.Key;
					console.log('data', err, data);
					$uibModalInstance.close($scope.form);					
				}
			});
		} else if($scope.file2) {
			var secondaryParams = { 
				Key: $scope.file2.name, 
				ContentType: $scope.file2.type, 
				Body: $scope.file2, 
				ServerSideEncryption: 'AES256' 
			};
			bucket.putObject(secondaryParams, function(err, data) {
				if(err) {
					// There Was An Error With Your S3 Config
					toastService.error(err.message);  
					isValid = false;
					return false;
				} else {
					$scope.form.secondary_image = 'https://s3.amazonaws.com/images.flashbid/' + secondaryParams.Key;
					console.log('secondary_data', err, data);
					$uibModalInstance.close($scope.form);					
				}
			});
		} else {
			$uibModalInstance.close($scope.form);	
		}			
	}

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);