angular.module('app')
.constant('mockItems', [{
        id: 1,
        title: 'Rolex Watch',
        image: 'http://img.com/img.png',
        type: 'a',
        ticketTotal: 30000,
        ticketSold: 29800,
        winnerId: 0,
        initialBid: 10,
        status: 'Active'
    }, {
        id: 2,
        title: 'Blue Watch',
        image: 'http://img.com/img.png',
        type: 'l',
        ticketTotal: 30000,
        ticketSold: 27800,
        winnerId: 0,
        initialBid: 10,
        status: 'Active'
    }, {
        id: 3,
        title: 'Pink Watch',
        image: 'http://img.com/img.png',
        type: 'l',
        ticketTotal: 30000,
        ticketSold: 0,
        winnerId: 0,
        initialBid: 10,
        status: 'Draft'
    }, {
        id: 4,
        title: 'Red Watch',
        image: 'http://img.com/img.png',
        type: 'a',
        ticketTotal: 30000,
        ticketSold: 30000,
        winnerId: 0,
        initialBid: 10,
        status: 'Flash'
    }, , {
        id: 5,
        title: 'Yellow Watch',
        image: 'http://img.com/img.png',
        type: 'l',
        ticketTotal: 30000,
        ticketSold: 30000,
        winnerId: 10,
        initialBid: 10,
        status: 'Complete'
    }, {
        id: 6,
        title: 'Purple Watch',
        image: 'http://img.com/img.png',
        type: 'a',
        ticketTotal: 30000,
        ticketSold: 30000,
        winnerId: 0,
        initialBid: 10,
        status: 'Complete'
    }]);