angular.module('app').controller('user.controller', ['$scope', 'http.service', 'userAPIURL', 'toast.service', '$uibModal',
    function ($scope, httpService, userAPIURL, toastService, $uibModal) {
        $scope.items = [];
        $scope.form = {
            page: 1,
            total: 1,
            numItems: 20,
            email: null,
            id: null,
            phone_number: null,
            first_name: null,
            last_name: null,
            role: null,
            status: null
        };

        $scope.vendors = [];
        $scope.mapping = {
            "Pending": 'Pendiente',
            "Active": 'Activo',
            "Suspend": 'Suspender',
            "Ban": 'Prohibición'
        };

        $scope.roleMapping = {
          "Member" : 'Miembro',
          "Vendor" : 'Vendedor',
          "Distributor" : 'Distribuidor',
          "Admin" : 'Administración',
          "System" : 'Sistema'
        };
        $scope.getUsers = function () {
            const queryStr = httpService.buildQuery($scope.form);
            httpService.doGet(userAPIURL.getAll + '?' + queryStr).then(function (users) {
                $scope.items = users.items;
                $scope.form.page = users.page;
                $scope.form.total = users.num_pages * $scope.form.numItems;
            })
        };

        $scope.logout = function (user) {
            httpService.doGet(userAPIURL.logout + '/' + user.id).then(function (user) {
                toastService.success('Usuario desconectado');
            })
        };

        $scope.edit = function (user) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'users/editUser.html',
                controller: 'edit.user.controller',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    user: function () {
                        return user;
                    }
                }
            });

            modalInstance.result.then(function (form) {
                form.id = user.id;
                httpService.doPost(userAPIURL.edit, form).then(function (result) {
                    if (result.statusCode == 403) {
                        toastService.error(result.message);
                    } else {
                        toastService.success('Actualizado');
                        $scope.getUsers();
                    }
                });
            });
        };

        $scope.addUser = function (isVendor) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'users/addUser.html',
                controller: 'add.user.controller',
                controllerAs: 'vm',
                size: 'lg'
            }),
                url = userAPIURL.create;
            if (isVendor) {
                url = userAPIURL.vendor;
            }
            modalInstance.result.then(function (form) {
                httpService.doPost(url, form).then(function (result) {
                    if (result.statusCode == 403) {
                        toastService.error(result.message);
                    } else {
                        toastService.success('Usuario agregado');
                        $scope.getUsers();
                    }
                });
            });
        };

        $scope.addVendor = function () {
            $scope.addUser(true);
        };
        $scope.addDistributor = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'users/addDistributor.html',
                controller: 'add.distributor.controller',
                controllerAs: 'vm',
                size: 'lg'
            });

            modalInstance.result.then(function (form) {
                httpService.doPost(userAPIURL.distributor, form).then(function (result) {
                    if (result.statusCode == 403) {
                        toastService.error(result.message);
                    } else {
                        toastService.success('Usuario agregado');
                        $scope.getUsers();
                    }
                });
            });
        }

        $scope.getVendors = function () {
            const queryStr = httpService.buildQuery({
                page: 1,
                total: 1,
                numItems: 10000,
                role: 'Distributor'
            });
            httpService.doGet(userAPIURL.getAll + '?' + queryStr).then(function (users) {
                $scope.vendors = users.items;
            })
        };
        $scope.pageChanged = function () {
            $scope.getUsers();
            $scope.getVendors();
        }
        $scope.getUsers();
        $scope.getVendors();
    }]).constant('userAPIURL', {
        getAll: '/v1/usersAll',
        edit: '/v1/useredit',
        view: '/v1/userView',
        create: '/v1/userscreate',
        vendor: '/v1/usersVendor',
        distributor: '/v1/userdistributor',
        logout: '/v1/logout'
    });