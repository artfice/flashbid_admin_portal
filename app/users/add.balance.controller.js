angular.module('app').controller('add.balance.controller', ['$scope', '$uibModalInstance', 
 function($scope, $uibModalInstance ) {
	$scope.form = {
		amount: 0
	};
	console.log($scope.form);
	$scope.submitForm = function (isValid) {
		if (!$scope.addBalanceForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	}

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);