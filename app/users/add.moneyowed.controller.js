angular.module('app').controller('add.moneyowed.controller', ['$uibModalInstance', '$scope', 
 function($uibModalInstance, $scope) {
	$scope.form = {
		amount: 0
	};

	$scope.submitForm = function (isValid) {
		if (!$scope.addBalanceForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	}

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);