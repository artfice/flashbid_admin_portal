angular.module('app').controller('user.profile.controller', ['$scope', 'http.service', 'userProfileAPIURL',
    '$stateParams', '$uibModal', 'toast.service', function ($scope, httpService, userProfileAPIURL, $stateParams, $uibModal, toastService) {
        $scope.profile = null; 
        $scope.mapping = {
            "Pending": 'Pendiente',
            "Active": 'Activo',
            "Suspend": 'Suspender',
            "Ban": 'Prohibición'
        };        

        $scope.roleMapping = {
          "Member" : 'Miembro',
          "Vendor" : 'Vendedor',
          "Distributor" : 'Distribuidor',
          "Admin" : 'Administración',
          "System" : 'Sistema'
        };        
        $scope.getProfile = function () {
            httpService.doGet(userProfileAPIURL.getOne + '/' + $stateParams.id).then(function (user) {
                $scope.profile = user;
            });
        };
        $scope.refund = function (){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'users/refund.html',
                controller: 'add.refund.controller',
                controllerAs: 'vm',
                size: 'lg'   
                });
                
                modalInstance.result.then(function (form) {
                    httpService.doPost(userProfileAPIURL.refund + '/' + $scope.profile.user.id, form).then(function(result) {
                        if (result.statusCode == 403) {
                            toastService.error(result.message);    
                        } else {
                            toastService.success('Usuario reembolsado');
                            $scope.getProfile();
                        }
                    });
                });       
        };

        $scope.addBalance = function(user) {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'users/addBalance.html',
                controller: 'add.balance.controller',
                controllerAs: 'vm',
                size: 'lg'   
                });

                modalInstance.result.then(function (form) {
                    form.id = user.id;
                    httpService.doPost(userProfileAPIURL.balance, form).then(function(result) {
                        if (result.statusCode == 403) {
                            toastService.error(result.message);    
                        } else {
                            toastService.success('Actualizado');
                            $scope.getProfile();
                        }
                    });
                });                   
        };            
        $scope.moneyOwned = function (){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'users/moneyowed.html',
                controller: 'add.moneyowed.controller',
                controllerAs: 'vm',
                size: 'lg'    
                });
                
                modalInstance.result.then(function (form) {
                    httpService.doPost(userProfileAPIURL.moneyowed + '/' + $scope.profile.user.id, form).then(function(result) {
                        if (result.statusCode == 403) {
                            toastService.error(result.message);    
                        } else {
                            toastService.success('Dinero liquidado debido');
                            $scope.getProfile();
                        }
                    });
                });  
        };
        $scope.getProfile();
    }]).constant('userProfileAPIURL', {
        getOne: '/v1/users',
        refund: '/v1/users/refund',
        refund: '/v1/users/refund',
        balance: '/v1/users/balance',
        moneyowed: '/v1/users/vendor'
    });