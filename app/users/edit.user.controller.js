angular.module('app').controller('edit.user.controller', ['$uibModalInstance', '$scope', 'user',
 function($uibModalInstance, $scope, user) {
	$scope.form = {
		password: '',
		first_name: user.first_name,
		last_name: user.last_name,
		email: user.email,
		phone_number: user.phone_number,
		status: user.status,
		role: user.role,
		bid_attempts: user.bid_attempts,
		group_name: user.group_name
	};

	$scope.submitForm = function (isValid) {
		if (!$scope.addUserForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	}

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);