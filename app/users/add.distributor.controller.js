angular.module('app').controller('add.distributor.controller', ['$uibModalInstance', '$scope', 
 function($uibModalInstance, $scope) {
	$scope.form = {
		password: '',
		profile_image: ''
	};

	$scope.submitForm = function (isValid) {
		if (!$scope.addUserForm.$invalid) {
			$uibModalInstance.close($scope.form);
		}
	}

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);